#!/bin/bash
EXEC=ICHelper
APP_HOME=$(cd "$(dirname $0)/..";pwd)
CMD="kill -TERM"
PID=$(cat $APP_HOME/var/run/gunicorn.pid)
$CMD $PID
