#!/bin/bash
EXEC=ICHelper
APP_HOME=$(cd "$(dirname $0)/..";pwd)
CMD="gunicorn"
nohuplog="$APP_HOME/var/log/nohup.out"
nohup $CMD -c $APP_HOME/etc/conf/gunicorn.pyc --chdir $APP_HOME app:app >> $nohuplog 2>&1 &
