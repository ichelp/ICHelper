import hashlib


def getMd5(pwd):
    pwd = hashlib.md5(bytes(pwd, encoding='utf-8'))
    return pwd.hexdigest()
