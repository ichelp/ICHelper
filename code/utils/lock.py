import datetime
import shelve
import threading
from time import sleep

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from utils import logger
# from parse import _login
from utils.checkChrome import checkVersionMatch
from .options import chromeOptions
from selenium.webdriver.chrome.service import Service
import os


class Singleton(object):
    def __call__(self, *args, **kwds):
        raise TypeError('instantiation is prohibited')


class DriverUtil(Singleton):
    _userName = None
    _password = None
    _flag = 'false'
    _instance = None
    _lock = threading.Lock()

    def __init__(self):
        # 检测google和驱动版本是否一致
        checkVersionMatch()
        self.lock = threading.Lock()
        # self.driver = _login('https://passport.hqew.com/login')
        service = Service()
        service.start()
        options = chromeOptions()
        self.driver = webdriver.Chrome(chrome_options=options)

        with open('.\\etc\\stealth.min.js-main\\stealth.min.js') as f:
            js = f.read()
        self.driver.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
            "source": js
        })

        # 启动redis
        main = ".\\etc\\redis\\redis-server.exe"
        # 异步执行 os.system() 这个方法是拥塞的
        os.popen(main)
        # 调起更新操作的exe(本地调试切换路径)
        os.popen('AutoUpdate.exe')
        # driver.get("https://www.ichelp.cn")
        # 设置一个智能等待
        self.wait = WebDriverWait(self.driver, 10)

    @classmethod
    def instance(cls):
        if cls._instance is None:
            cls._lock.acquire()
            if cls._instance is None:
                cls._instance = cls()
                # cls._instance.driver = login('https://www.findchips.com/signin')
            cls._lock.release()
        return cls._instance

    def open(self, url):
        js = 'window.open("%s")' % url
        handle = ''
        self.lock.acquire()
        try:
            self.driver.execute_script(js)
            all_handles = self.driver.window_handles
            handle = all_handles[-1]
            # 遮阴罩问题
            path = self.driver.find_elements_by_id('J-zhishu')
            js = 'arguments[0].style="display: none; inset: 0px; position: absolute;"'
            if len(path) > 0:
                self.driver.execute_script(js, path[0])
        except:
            # self.driver.close()
            self.driver.switch_to.window(all_handles[-1])
            logger.exception("DriverUtil open tab catch an exception")
        finally:
            # 切换回 0 号窗口
            all_handles = self.driver.window_handles
            self.driver.switch_to.window(all_handles[0])
            # 修改完成，释放锁
            self.lock.release()
        return handle

    def close(self, handle):
        self.lock.acquire()
        html = ''
        try:
            self.driver.switch_to.window(handle)
            html = self.driver.page_source
            # 关闭窗口
            self.driver.close()
        except:
            logger.exception("DriverUtil close tab catch an exception")
        finally:
            # 切换回 0 号窗口
            all_handles = self.driver.window_handles
            self.driver.switch_to.window(all_handles[0])
            # 修改完成，释放锁
            self.lock.release()
        return html

    def htmlHqew(self, handle):
        self.lock.acquire()
        html = ''
        try:
            self.driver.switch_to.window(handle)
            html = self.driver.page_source
        except:
            logger.exception("DriverUtil htmlHqew tab catch an exception")
        finally:
            # 切换回 0 号窗口

            all_handles = self.driver.window_handles
            self.driver.switch_to.window(all_handles[0])
            # 修改完成，释放锁
            self.lock.release()
        return html

    def closeHqew(self, handle):
        self.lock.acquire()
        currentUrl = ''
        try:
            self.driver.switch_to.window(handle)
            currentUrl = self.driver.current_url
            self.driver.close()
        except:
            logger.exception("DriverUtil closeHqew tab catch an exception")
        finally:
            # 切换回 0 号窗口
            all_handles = self.driver.window_handles
            self.driver.switch_to.window(all_handles[0])
            # 修改完成，释放锁
            self.lock.release()
        return currentUrl

    def mouseSuspended(self, handle, xpath):
        self.lock.acquire()
        try:
            self.driver.switch_to.window(handle)
            # 遮阴罩问题
            path = self.driver.find_elements_by_id('J-zhishu')
            js = 'arguments[0].style="display: none; inset: 0px; position: absolute;"'
            if len(path) > 0:
                self.driver.execute_script(js, path[0])

            MoveElement = self.driver.find_element_by_xpath(('//*[@id="r{}"]/td[2]/p/a').format(xpath))
            # 将鼠标移到MoveElement
            Action = ActionChains(self.driver)
            Action.move_to_element(MoveElement).perform()
            html = self.driver.page_source
        except Exception as e:
            logger.error('错误异常' + e)
        finally:
            self.lock.release()
        return html

    def loginHqew(self, username, password, handle, requestUrl):
        self.lock.acquire()
        html = ""
        url = ""
        message = ""
        code = 200
        try:
            self.driver.switch_to.window(handle)
            u = self.wait.until(EC.presence_of_element_located((By.ID, 'J_loginName')))
            u.clear()
            u.send_keys(username)
            p = self.wait.until(EC.presence_of_element_located((By.ID, 'J_loginPsw')))
            p.clear()
            p.send_keys(password)
            b = self.wait.until(EC.presence_of_element_located((By.ID, 'J_btnLogin')))
            # 当前登录页面的url
            loginUrl = self.driver.current_url
            b.click()
            sleep(1)
            # 精确查询需要
            self.driver.get(requestUrl)

            # 遮阴罩问题
            path = self.driver.find_elements_by_id('J-zhishu')
            js = 'arguments[0].style="display: none; inset: 0px; position: absolute;"'
            if len(path) > 0:
                self.driver.execute_script(js, path[0])

            html = self.driver.page_source
            # 判断登录状态
            currentUrl = self.driver.current_url
            if requestUrl == currentUrl:
                # 登录成功
                # 记录登录的账户,密码 状态
                DriverUtil._userName = username
                DriverUtil._password = password
                DriverUtil._flag = 'true'
                contentBs = BeautifulSoup(html, features="lxml")
                resultList = contentBs.find("div", "resultList")
                if resultList is not None:
                    return html, currentUrl, 200, message
                else:
                    targetUrl = 'https:' + loginUrl.split("backpathurl=")[1]
                    self.driver.get(targetUrl)
                    # 等待页面加载完成
                    self.wait.until(EC.presence_of_element_located((By.CLASS_NAME, 'bread-item')))
                    return self.driver.page_source, targetUrl, 200, message
            elif 'https://passport.hqew.com/' in currentUrl:
                # 登录失败
                message = "用户名或密码错误，请重新输入！"
                return '', loginUrl, 401, message
        except:
            logger.exception("login hqew exception")
        finally:
            self.lock.release()
        return html, url, code, message

    def quit(self):
        self.lock.acquire()
        self.driver.quit()
        self.lock.release()

    # 刷新原页面
    def refreshHqew(self, handle, requestUrl):
        self.driver.switch_to.window(handle)
        self.driver.get(requestUrl)
        self.driver.refresh()

        # 遮阴罩问题
        path = self.driver.find_elements_by_id('J-zhishu')
        js = 'arguments[0].style="display: none; inset: 0px; position: absolute;"'
        if len(path) > 0:
            self.driver.execute_script(js, path[0])

        self.wait.until(EC.presence_of_element_located((By.CLASS_NAME, 'bread-item')))
        pageSource = self.driver.page_source
        contentBs = BeautifulSoup(pageSource, features="lxml")
        return contentBs

    def headLoginOpen(self, url):
        self.lock.acquire()
        try:
            self.driver.switch_to.window(self.driver.window_handles[0])
            self.driver.get(url)
        except:
            logger.exception("headOpen tab catch an exception")
        finally:
            self.lock.release()
            return self.driver



    def openSzlcsc(self, url):
        js = 'window.open("%s")' % url
        handle = ''
        self.lock.acquire()
        try:
            self.driver.execute_script(js)
            all_handles = self.driver.window_handles
            print(all_handles)
            handle = all_handles[-1]
        except Exception:
            logger.error('')
        finally:
            # 修改完成，释放锁
            self.lock.release()
        return handle

    def closeSzlcsc(self, handle):
        self.lock.acquire()
        self.driver.switch_to.window(handle)
        # 将页面滚动条拖到底部 触发加载产品图片
        js_bottom = "var q =document.documentElement.scrollTop=10000"  # 页面底部的固定写法
        self.driver.execute_script(js_bottom)  # 在python中执行这一段脚本
        sleep(0.3)
        html = self.driver.page_source
        # 关闭窗口
        self.driver.close()

        # 切换回 0 号窗口
        all_handles = self.driver.window_handles
        self.driver.switch_to.window(all_handles[0])
        self.lock.release()
        return html

    def closeShadeHood(self):
        try:
            # 实体认证遮阴罩
            path = self.driver.find_elements_by_id('j-strz-close')
            if len(path) > 0:
                path[0].click()
        except:
            logger.error('')


DriverUtil.instance()
