from selenium import webdriver

def chromeOptions():
    options = webdriver.ChromeOptions()
    # 设置谷歌浏览器不加载图片
    # No_Image_loading = {"profile.managed_default_content_settings.images": 2}
    # options.add_experimental_option("prefs", No_Image_loading)

    prefs = {'profile.managed_default_content_settings.images': 2}  # 不加载图片, 提升速度
    prefs.update({"credentials_enable_service": False, "profile.password_manager_enabled": False})  # 登录时关闭弹出的密码保存提示框
    prefs.update({'profile.default_content_setting_values': {'notifications': 2}})  # 禁用浏览器弹窗
    options.add_experimental_option('prefs', prefs)


    # 无界面模式浏览器启动
    options.add_argument('--headless')

    # 设置浏览器参数
    options.add_experimental_option('useAutomationExtension', False)
    options.add_experimental_option('excludeSwitches', ['enable-automation'])

    # 设置谷歌浏览器不关闭
    options.add_experimental_option("detach", True)

    # 解决DevToolsActivePort文件不存在的报错
    options.add_argument('--no-sandbox')


    options.add_argument("--disable-blink-features=AutomationControlled")

    options.add_argument('user-agent=Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; .NET4.0C; .NET4.0E; .NET CLR 2.0.50727; .NET CLR 3.0.30729; .NET CLR 3.5.30729; InfoPath.3; rv:11.0) like Gecko')
    options.add_argument('--disable-gpu')
    options.add_argument('--disable-dev-shm-usage')
    # 停止页面的不必要加载
    options.page_load_strategy = 'eager'
    return options
