import json
import winreg
import re
import urllib.parse
import zipfile
import os
import sys
import subprocess
from utils.logging import logger

# 匹配前3位版本号的正则表达式
version_re = re.compile(r'^[1-9]\d*\.\d*.\d*')
base_dir = os.path.dirname(os.path.dirname(__file__))
dir_path = os.path.dirname(os.path.realpath(__file__))


# 获取谷歌版本
def getChromeVersion():
    try:
        # 从注册表中获得版本号
        key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, r'Software\Google\Chrome\BLBeacon')

        chromeVersion, type = winreg.QueryValueEx(key, 'version')

        logger.info('Current Chrome Version: {}'.format(chromeVersion))
        # 返回前3位版本号
        return version_re.findall(chromeVersion)[0]
    except WindowsError as e:
        logger.info('check Chrome failed:{}'.format(e))


# 获取驱动的版本 absPath(驱动的绝对路径)
def getDriverVersion(absPath):
    cmd = r'{} --version'.format(absPath)  # 拼接成cmd命令
    try:
        # 执行cmd命令并接收命令回显
        out, err = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
        out = out.decode('utf-8')
        # 拆分回显字符串，获取版本号
        driverVersion = out.split(' ')[1]
        logger.info('Current chromedriver Version:{}'.format(driverVersion))
        return version_re.findall(driverVersion)[0]
    except IndexError as e:
        logger.info('check chromedriver failed:{}'.format(e))
        return 0


# 检测版本并下载
def checkVersionMatch():
    logger.info("检测版本并下载")
    # 谷歌驱动的路径
    absPath = '.\\chromedriver.exe'
    # absPath = 'F:\ICHelper\code\dist\chromedriver.exe'
    # 获取谷歌的版本
    chrome_version = getChromeVersion()
    # 获取谷歌驱动的版本
    driver_version = getDriverVersion(absPath)

    if chrome_version == driver_version:
        # 若匹配，在命令行窗口提示下面的信息
        logger.info('Chrome and chromedriver 版本一致无需更新')
        return
    else:
        # 若不匹配，走下面的流程去下载chromedriver
        version = chrome_version
        # 获取chromeDriver版本号的url
        url = 'https://registry.npmmirror.com/-/binary/chromedriver/'
        # 下载文件的保存路径，与chromedriver同级
        save_d = os.path.dirname(absPath)
        # call下载文件的方法
        downLoadDriver(version, url, save_d)


# 下载匹配的驱动
def downLoadDriver(version, url, save_d):
    # 访问淘宝镜像首页
    rep = urllib.request.urlopen(url).read().decode('utf-8')
    directory = json.loads(rep)
    # 获取期望的文件夹（版本号）
    match_list = []
    for i in directory:
        v = i['name'].replace('/', '')
        if version in v and not 'LATEST_RELEASE' in v:
            match_list.append(i)

    if len(match_list) == 0:
        return

        # 拼接出下载路径
    downUrl = 'https://registry.npmmirror.com/-/binary/chromedriver/' + match_list[-1][
        'name'] + 'chromedriver_win32.zip'
    logger.info('will download {}'.format(downUrl))

    # 指定下载的文件名和保存位置
    file = os.path.join(save_d, os.path.basename(downUrl))
    logger.info('will saved in {}'.format(file))

    # 开始下载，并显示下载进度(progressFunc)
    urllib.request.urlretrieve(downUrl, file)

    # 下载完成后解压
    zFile = zipfile.ZipFile(file, 'r')
    for fileM in zFile.namelist():
        zFile.extract(fileM, os.path.dirname(file))
    zFile.close()
    # 解压完成后删除zip文件
    os.remove(file)
    logger.info('解压完成后删除zip文件')


def progressFunc(blocknum, blocksize, totalsize):
    '''作回调函数用
    @blocknum: 已经下载的数据块
    @blocksize: 数据块的大小
    @totalsize: 远程文件的大小
    '''
    percent = 100.0 * blocknum * blocksize / totalsize

    if percent > 100:
        percent = 100
    downsize = blocknum * blocksize

    if downsize >= totalsize:
        downsize = totalsize

    s = "%.2f%%" % (percent) + "====>" + "%.2f" % (downsize / 1024 / 1024) + "M/" + "%.2f" % (
            totalsize / 1024 / 1024) + "M \r"
    sys.stdout.write(s)
    sys.stdout.flush()

    if percent == 100:
        logger.info('down chromeDriver已完成')
