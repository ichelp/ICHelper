# 定时任务相关
import json
import re
from time import sleep

import requests
from bs4 import BeautifulSoup

from utils import logger, DriverUtil

from pytypes import Product
from utils import DriverUtil

product = Product(batchNo='', brand='', companyInfo='', contact='', jumpUrl='', markerNo='', num='', package='')
url = 'https://so.szlcsc.com/global.html?k='


# 定时检测hqew登录状态
def _schedulerLoginHqew():
    try:
        instance = DriverUtil.instance()
        username = instance._userName
        password = instance._password
        if username == None or password == None:
            return
        driver = instance.headLoginOpen('https://www.hqew.com/')
        # 判断登录状态
        paths = driver.find_elements_by_xpath('//*[@id="J_login"]')
        if len(paths) > 0:
            if paths[0].text == '请登录，免费注册':

                driver.get('https://passport.hqew.com/login')
                sleep(5)

                u = driver.find_elements_by_xpath('//*[@id="J_loginName"]')
                if len(u) == 0:
                    return
                else:
                    u[0].clear()
                u[0].send_keys(username)

                p = driver.find_elements_by_xpath('//*[@id="J_loginPsw"]')
                if len(p) == 0:
                    return
                else:
                    p[0].clear()
                p[0].send_keys(password)

                b = driver.find_elements_by_xpath('//*[@id="J_btnLogin"]')
                if len(b) == 0:
                    return
                else:
                    b[0].click()

                sleep(5)
                # 判断登录状态
                if 'https://ibsv3.hqew.com/' in driver.current_url:
                    instance._flag = 'true'
                    logger.info("登录状态")
            else:
                instance._flag = 'true'
                logger.info("登录状态")
    except:
        logger.exception("定时 login hqew exception")


# 定时查询lcsc的料号信息的变化
def _schedulerParseSzlcsc():
    # 发送http请求获取料号信息
    data = requests.post('http://106.55.245.170:8081/icband-admin/api/marker/getRedis').json()
    if data['code'] != 200:
        return
    dataStr = data['data']
    if dataStr is None:
        return
    jsonData = json.loads(dataStr)
    # 料号id
    markerId = jsonData['markerId']
    # 料号
    marker = jsonData['marker']
    # 封装
    encapsulation = jsonData['encapsulation']
    # 品牌
    brandr = jsonData['brand']

    # 获取html页面
    driver = DriverUtil.instance()
    handle = driver.openSzlcsc(url + marker)
    html = driver.closeSzlcsc(handle)
    # 获取文本
    contentBs = BeautifulSoup(html, features="lxml")
    if not contentBs:
        return
    shopList = contentBs.find('div', 'home-scroll-img-mark')
    if shopList is None:
        return
    else:
        try:
            tbodyList = shopList.findAll("tbody")
            if tbodyList is None:
                return
            else:
                for i in range(len(tbodyList)):
                    tbody = tbodyList[i]
                    tdTwo01 = tbody.find("div", "two-01")
                    tdTwo01List = tdTwo01.findAll('li', 'li-ellipsis')
                    package = tdTwo01List[2]
                    if package is not None:
                        package = package.text.replace("封装：", '').replace("\n", '').replace(" ", '')

                    brand = tdTwo01List[1].find('a')
                    if brand is not None:
                        brand = brand.text.replace("\n", '').replace(" ", '')

                    # 封装和品牌相同
                    if package == encapsulation and brand == brandr:

                        tdThree = tbody.find("div", "three")
                        price = ''
                        unit = ''
                        if tdThree is None:
                            pass
                        else:
                            tdThreeLi = tdThree.findAll('div', 'price-warp price-warp-local')
                            if len(tdThreeLi) > 0:
                                priceStr = tdThreeLi[len(tdThreeLi) - 1].text.replace('\n', '').replace(' ', '').split(
                                    '：')
                                unit = priceStr[0].replace('+', '')
                                price = priceStr[1].replace('￥', '')

                        threeChange = tbody.find("div", "three-change")
                        num = ''
                        if threeChange is None:
                            pass
                        else:
                            finput = threeChange.find("ul", 'finput')
                            liList = finput.findAll('li')
                            if liList is not None and len(liList) >= 2:
                                jsStock = liList[0].find('div', 'stock-nums-js').text.replace("\n", '').replace(" ", '')
                                num = num + jsStock + ';'
                                gdStock = liList[0].find('div', 'stock-nums-gd').text.replace("\n", '').replace(" ", '')
                                num = num + gdStock + ';'
                            for j in range(len(liList)):
                                if j == 0:
                                    pass
                                else:
                                    num = num + liList[j].text.replace("\n", '').replace(" ", '') + ';'
                                    num = num[:-1].replace(";;", ';')
                            num = sum([int(x) for x in re.findall("\d+", num)])
                        # 调用http存储数据接口
                        data = {
                            'markerId': markerId,
                            'num': num,
                            'price': price,
                            'unit': unit,
                        }
                        data = requests.post('http://106.55.245.170:8081/icband-admin/api/marker/getData',
                                             json=data).json()
                        if data['code'] != 200:
                            print("数据未存储成功")
                            # 数据未消费成功 返回给redis
                            requests.get(
                                'http://106.55.245.170:8081/icband-admin/api/marker/updateStatus/' + str(markerId))
                            return
                    else:
                        continue
        except:
            requests.get('http://106.55.245.170:8081/icband-admin/api/marker/updateStatus/' + str(markerId))
            return
