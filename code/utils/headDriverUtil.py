from time import sleep

from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC, wait
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from utils import logger


def headOpen(url):
    options = chromeOptions()
    driver = webdriver.Chrome(chrome_options=options)
    # 绕过检测
    with open('.\\etc\\stealth.min.js-main\\stealth.min.js') as f:
        js = f.read()
    driver.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
        "source": js
    })

    js = 'window.open("%s")' % url
    try:
        driver.execute_script(js)
        wait = WebDriverWait(driver, 15)
        wait.until(EC.presence_of_element_located((By.CLASS_NAME, 'bread-item')))
    except:
        logger.exception("headOpen tab catch an exception")
    finally:
        driver.quit()


def chromeOptions():
    options = webdriver.ChromeOptions()
    # 设置浏览器参数
    options.add_experimental_option('useAutomationExtension', False)
    options.add_experimental_option('excludeSwitches', ['enable-automation'])
    # 设置谷歌浏览器不关闭
    options.add_experimental_option("detach", True)
    # 解决DevToolsActivePort文件不存在的报错
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    options.add_argument("--disable-blink-features=AutomationControlled")
    # 停止页面的不必要加载
    options.page_load_strategy = 'eager'
    return options
