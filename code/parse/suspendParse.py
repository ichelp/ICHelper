import datetime

from selenium.webdriver import ActionChains
from bs4 import BeautifulSoup

# 获取悬浮框并解析
from utils.logging import logger
from pytypes import Sort, Brand, CompanyInfo


def suspendParse(driverUtil, handle, xpath):
    start = datetime.datetime.now().timestamp()

    companyInfo = CompanyInfo(addres='', brandInfo=[], companyHref='', companyName='', invoice='',
                              merchantqualification='', phone='', qq='', qualificationYears='', shipmentNum='',
                              sortInfo=[], spotCheck='', strzimg='', telList='')

    driverUtil.closeShadeHood()

    html = driverUtil.mouseSuspended(handle, xpath)
    # 获取文本
    contentBs = BeautifulSoup(html, features="lxml")
    card = contentBs.find("div", id="card")
    # 公司名称
    companyName = card.find('a', id='name').text
    companyInfo.companyName = companyName

    # 公司网址
    companyHref = card.find('a', id='name')['href']
    companyInfo.companyHref = companyHref

    # 电话
    telList = ''
    phoneList = card.find("ul", id='phone').findAll('li')
    if len(phoneList) > 0:
        for i in range(len(phoneList)):
            liPhone = phoneList[i]
            spanPhone = liPhone.findAll('span')
            if len(spanPhone) == 0:
                continue
            for j in range(len(spanPhone)):
                if spanPhone[j]['class'][0] != 'n':
                    telList += spanPhone[j].text
                else:
                    telList = telList + "\r" + spanPhone[j].text + ";"
    companyInfo.telList = telList[:-1].replace('电话：', '')

    # 手机号
    phone = card.find("li", id='mphone').text
    companyInfo.phone = phone.replace("手机：", "")

    # qq号
    qq = ''
    mqq = card.find("li", id='mqq').findAll('a')
    if len(mqq) > 0:
        for k in range(len(mqq)):
            if k != len(mqq) - 1:
                qqHref = mqq[k]['href']
                contacts = mqq[k].text.replace(' ', '')
                qq = qq + qqHref + "-" + contacts + ';'
            else:
                qqHref = mqq[k]['href']
                contacts = mqq[k].text
                qq = qq + qqHref + "-" + contacts
    companyInfo.qq = qq.replace("Q Q：", '')

    # 地址
    address = card.find("li", id='address').text
    companyInfo.address = address.replace("地址：", "")

    # 实体认证
    strzimg = ''
    imgList = card.findAll('div', 'img-box')
    if len(imgList) > 0:
        for j in range(len(imgList)):
            if j != len(imgList) - 1:
                strzimg += "https:" + imgList[j]['data-src']
                strzimg += ';'
            else:
                strzimg += "https:" + imgList[j]['data-src']
    companyInfo.strzimg = strzimg

    # right部分
    rightDiv = card.find('div', 'right')

    liList = rightDiv.findAll('li')
    if len(liList) == 6:
        # 商家资质Merchantqualification

        merchantqualification = ''
        # 会员
        member = liList[0].find('a', id='pack1')
        if len(member['class']) > 0 and 'inline-block' in member['style']:
            merchantqualification += member['class'][0] + ','

        # 诚信通
        cx = liList[0].find('a', id='cx')
        if len(cx['class']) > 0 and 'inline-block' in cx['style']:
            merchantqualification += cx['class'][1] + ','

        # BCP现货认证
        bcp = liList[0].find('a', id='bcp')
        if len(bcp['class']) > 0 and 'inline-block' in bcp['style']:
            merchantqualification += bcp['class'][1] + ','

        # BCP现货认证
        iscpyz = liList[0].find('a', id='iscp-yz')
        if len(iscpyz['class']) > 0 and 'inline-block' in iscpyz['style']:
            merchantqualification += iscpyz['class'][0] + ','

        # ISCP现货认证
        iscpxh = liList[0].find('a', id='iscp-xh')
        if len(iscpxh['class']) > 0 and 'inline-block' in iscpxh['style']:
            merchantqualification += iscpxh['class'][0] + ','

        # ISCP正品认证
        iscpzp = liList[0].find('a', id='iscp-zp')
        if len(iscpzp['class']) > 0 and 'inline-block' in iscpzp['style']:
            merchantqualification += iscpzp['class'][0] + ','

        # 品牌认证
        pp1 = liList[0].find('a', id='pp1')
        if len(pp1['class']) > 0 and 'inline-block' in pp1['style']:
            merchantqualification += pp1['class'][1] + ','

        # 代理
        daili = liList[0].find('a', id='daili')
        if len(daili['class']) > 0 and 'inline-block' in daili['style']:
            merchantqualification += daili['class'][0] + ','
        companyInfo.merchantqualification = merchantqualification[:-1].replace('商家资质：', '')

        # 资质年限
        qualificationYears = ''
        if rightDiv.find("li", id='zznx1') is not None:
            # 会员年限
            year1card = liList[1].find('a', id='i-year1-card')
            if len(year1card['class']) > 0 and 'inline-block' in year1card['style']:
                em = year1card.find('em').text
                num = year1card.find('i').text
                qualificationYears = year1card['class'][0] + ':' + em + ':' + num + ';'

            # 现货年限
            yearxh1card = liList[1].find('a', id='i-xh-year1-card')
            if len(yearxh1card['class']) > 0 and 'inline-block' in yearxh1card['style']:
                em = yearxh1card.find('em').text
                num = yearxh1card.find('i').text
                qualificationYears = yearxh1card['class'][0] + ':' + em + ':' + num + ';'

            # 原装年限
            yearyz1card = liList[1].find('a', id='i-yz-year1-card')
            if len(yearyz1card['class']) > 0 and 'inline-block' in yearyz1card['style']:
                em = yearyz1card.find('em').text
                num = yearyz1card.find('i').text
                qualificationYears = yearyz1card['class'][0] + ':' + em + ':' + num + ';'

            # BCP年限
            yearbcp1card = liList[1].find('a', id='i-bcp-year1-card')
            if len(yearbcp1card['class']) > 0 and 'inline-block' in yearbcp1card['style']:
                em = yearbcp1card.find('em').text
                num = yearbcp1card.find('i').text
                qualificationYears = yearbcp1card['class'][0] + ':' + em + ':' + num + ';'

            # 品牌年限
            yearpp1card = liList[1].find('a', id='i-pp-year1-card')
            if len(yearpp1card['class']) > 0 and 'inline-block' in yearpp1card['style']:
                em = yearpp1card.find('em').text
                num = yearpp1card.find('i').text
                qualificationYears = yearpp1card['class'][0] + ':' + em + ':' + num + ';'

        # 代理年限
        dlyear1card = rightDiv.find('a', id='i-dl-year1-card')
        if len(dlyear1card['class']) > 0 and 'inline-block' in dlyear1card['style']:
            em = dlyear1card.find('em').text
            num = dlyear1card.find('i').text
            qualificationYears = dlyear1card['class'][0] + ':' + em + ':' + num + ';'

        # 正品年限
        zpyear1card = rightDiv.find('a', id='i-zp-year1-card')
        if len(zpyear1card['class']) > 0 and 'inline-block' in zpyear1card['style']:
            em = zpyear1card.find('em').text
            num = zpyear1card.find('i').text
            qualificationYears = zpyear1card['class'][0] + ':' + em + ':' + num + ';'

        companyInfo.qualificationYears = qualificationYears[:-1].replace('资质年限：', '')
        # 发票
        invoice = liList[2].text
        companyInfo.invoice = invoice.replace('开       票：', '')
        # 出货量
        shipmentNum = liList[3].text
        companyInfo.shipmentNum = shipmentNum.replace('出 货 量：', '')
        # 抽查
        spotCheck = liList[4].text
        companyInfo.spotCheck = spotCheck.replace('现货抽查：', '')

    # 饼状图部分
    # 经营品牌
    charPp = card.find("div", id='J-char-info-pp')
    if charPp is not None:
        charWrapperList = charPp.findAll('span', 'char-s-l')
        if len(charWrapperList) > 0:
            for z in range(len(charWrapperList)):
                brand = Brand(name='', percent='')
                brandName = charWrapperList[z].text
                brand.name = brandName
                percentage = charWrapperList[z].parent()[3].text
                brand.percent = percentage
                companyInfo.brandInfo.append(brand.as_dict())

    # 经营品类
    charPl = card.find("div", id='J-char-info-pl')
    if charPl is not None:
        charWrapperList = charPl.findAll('span', 'char-s-l')
        if len(charWrapperList) > 0:
            for z in range(len(charWrapperList)):
                sortName = charWrapperList[z].text
                percentage = charWrapperList[z].parent()[3].text
                sort = Sort(name='', percent='')
                sort.name = sortName
                sort.percent = percentage
                companyInfo.sortInfo.append(sort.as_dict())
    end = datetime.datetime.now().timestamp()
    logger.info("单个悬浮耗时:" + str(end - start) + "毫秒")
    return companyInfo.as_dict()


if __name__ == "__main__":
    suspendParse("https://s.hqew.com/STM32F103C8T6.html")
