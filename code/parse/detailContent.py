import datetime

from bs4 import BeautifulSoup
from utils import logger
from utils.lock import DriverUtil
from time import sleep


def _getDetailContent(url):
    # 获取实例
    driverUtil = DriverUtil.instance()
    # 打开窗口
    handle = driverUtil.open(url)
    f = datetime.datetime.now()
    html = _get_html(handle, driverUtil, url)
    c = datetime.datetime.now()
    logger.info("获取html页面源码耗时：" + str(c - f))
    # 获取文本
    contentBs = BeautifulSoup(html, features="lxml")
    d = datetime.datetime.now()
    logger.info("解析html页面源码contentBs耗时：" + str(d - c))
    if not contentBs:
        logger.info("未获取到contentBs..")
        return contentBs, driverUtil, handle
    else:
        logger.info("获取到contentBs和driver")
        return contentBs, driverUtil, handle


def _get_html(handle, driverUtil, url):
    # 获取html
    html = ''
    num = 0
    while num < 20 and (len(html) <= 0 or not html.__contains__('title') or not html.__contains__('resultList') or not html.__contains__('亲')):
        num = num + 1
        if html.__contains__('用户登录'):
            logger.info("触发用户登录操作")
            break
        if html.__contains__('抱歉'):
            logger.info('暂无数据')
            break
        if num % 20 == 0:
            logger.info("触发网页刷新操作")
            # 关闭窗口
            url = driverUtil.closeHqew(handle)
            # 重新加载页面
            handle = driverUtil.open(url)
            pass
        sleep(0.1)
        html = driverUtil.htmlHqew(handle)
        logger.info(url + '第' + str(num) + '次获取html源码')
    return html
