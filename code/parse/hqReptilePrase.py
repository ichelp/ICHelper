# -*- coding:utf-8 -*-

# 解析页面
import json
from concurrent.futures import ThreadPoolExecutor
import time
import redis
from parse.detailContent import _getDetailContent
from utils import headDriverUtil
from utils.logging import logger
from parse.suspendParse import suspendParse
import pytypes
from utils.md5 import getMd5

pool = redis.ConnectionPool(host='127.0.0.1', port=6379)
redisClient = redis.Redis(connection_pool=pool)

executor = ThreadPoolExecutor(5)

product = pytypes.Product(batchNo='', brand='', contact='', jumpUrl='', markerNo='', num='', package='', key='')


def getAndParseContent(requestUrl, redisCacheUrl):
    contentBs, driverUtil, handle = _getDetailContent(requestUrl)
    titleCon = contentBs.find("title")
    if titleCon is None:
        return [], 200, handle
    title = titleCon.text
    if title == "请完成安全验证" or title == '搜索请求验证':
        logger.info("需要安全验证")
        # 打开有头新的浏览器
        headDriverUtil.headOpen(requestUrl)

        # 刷新无头浏览器的原页面
        contentBs = driverUtil.refreshHqew(handle, requestUrl)
        title = contentBs.find("title").text
        if title == "请完成安全验证":
            return [], 403, handle
        else:
            return _parseDetailContent(contentBs, driverUtil, handle, redisCacheUrl)
        # return productList, 403, handle
    elif "用户登录 - 华强电子网" == title:
        logger.info("需要登录")
        return [], 401, handle
    else:
        return _parseDetailContent(contentBs, driverUtil, handle, redisCacheUrl)


def _parseDetailContent(contentBs, driverUtil, handle, redisCacheUrl=None):
    logger.info("开始解析list页面")
    productList = []

    # 获取到list节点
    resultList = contentBs.find("div", "resultList")
    if not resultList:
        logger.error("not found div resultList")
        driverUtil.closeHqew(handle)
        return productList, 200, handle
    tableList = resultList.find("table", "list-table")
    if not tableList:
        logger.error("not found table list-table")
        driverUtil.closeHqew(handle)
        return productList, 200, handle
    # 解析頁面
    trlist = tableList.select("tr")
    if trlist and len(trlist) < 2:
        logger.error("tr list len less than 2")
        driverUtil.closeHqew(handle)
        return productList, 200, handle
    # 循环跳过前两条数据(脏数据)
    for i in range(len(trlist)):
        if i < 1:
            continue
        try:
            # 每一行的数据
            trContent = trlist[i]

            # 获取每一节点的数据
            # 供应商
            supplier = trContent.find('a')['cname']
            if '华强商城' in supplier:
                continue
            uid = trContent.find('a')['uid']
            if len(supplier) == 0:
                product.supplier = ""
            else:
                product.supplier = supplier + ';' + uid

            labelText = trContent.find('div', 'company-row2')
            if labelText is not None:
                label = labelText.find('a')['class'][0]
                if len(label) == 0:
                    product.label = ""
                else:
                    product.label = label

            # 点击跳转链接
            jumpUrl = trContent.find('a')['href']
            if len(jumpUrl) == 0:
                product.jumpUrl = ""
            else:
                product.jumpUrl = jumpUrl

            # 型号
            markerNoInfo = trContent.find('div', 'list-pro').findAll('a')
            markerNo = markerNoInfo[0].text
            if len(markerNoInfo) >= 1:
                if len(markerNo) == 0:
                    product.markerNo = ""
                else:
                    product.markerNo = markerNo
            if len(markerNoInfo) >= 3:
                if markerNoInfo[1]['rel'][0] == 'nofollow':
                    product.label = product.label + ";" + markerNoInfo[1]['class'][0]

            # 库存/数量markerNoInfo[1]
            num = trContent.find('td', 'td-stockNum').find('p', 'over')['title']
            if len(num) == 0:
                product.num = ""
            else:
                product.num = num

            # 制造商/品牌
            brand = trContent.find('td', 'td-brand').find('div', 'list-pro')['title']
            if len(brand) == 0:
                product.brand = ""
            else:
                product.brand = brand

            # 批次号
            batchNo = trContent.findAll('p', 'over')[3]['title']
            if len(batchNo) == 0:
                product.batchNo = ""
            else:
                product.batchNo = batchNo

            # 产品参数/详情
            productDetail = trContent.find('td', 'td-param').find('div', 'list-pro')['title']
            if len(productDetail) == 0:
                product.productDetail = ""
            else:
                product.productDetail = productDetail

            # 仓位
            position = ''
            location = trContent.findAll('td')[11].text.replace('\n', '').replace(' ', '')
            if len(location) > 0:
                position = '仓位:' + location + " ;"
            # 交易说明/描述
            remark = trContent.findAll('td')[12].find('div', 'list-pro')['title']
            if len(remark) == 0:
                product.remark = ""
            else:
                product.remark = position + remark
            # 日期
            date = trContent.findAll('p', 'over')[6].text.replace('  ', '')
            if len(date) == 0:
                product.date = ""
            else:
                product.date = date
            # 封装
            package = trContent.findAll('p', 'over')[4].text
            if len(package) == 0:
                product.package = ''
            else:
                product.package = package

            contact = trContent.find('a', 'customerqq dr customerqq_pop qqInfo')
            if contact is None:
                product.contact = ''
            else:
                product.contact = contact['href']
            key = getMd5(str(product))
            product.key = product.markerNo + "--" + key
            productList.append(product.as_dict())
        except:
            logger.info("第" + str(i) + "行报错了")
            continue
    # 异步处理
    if redisCacheUrl is not None:
        executor.submit(__asyncTask, productList, driverUtil, handle)
    logger.info("解析list页面完成")
    # 缓存数据
    if len(productList) > 0:
        now = time.time()
        respBody = pytypes.ListHqewProductsRespBody(code=200, message="ok", result=productList, timestamp=int(now))
        redisClient.set(redisCacheUrl, json.dumps(respBody.as_dict(), indent=2, ensure_ascii=False), ex=900)
    return productList, 200, handle


# 异步处理悬浮框数据及存储redis
def __asyncTask(productList, driverUtil, handle):
    logger.info("异步处理悬浮框数据开始")
    if len(productList) > 0:
        for i in range(len(productList)):
            try:
                product = productList[i]
                # 悬浮框部分内容
                supplier = product['supplier']
                # xpath
                companyInfo = suspendParse(driverUtil, handle, supplier.split(';')[1])
                redisClient.set(product['key'], json.dumps(companyInfo, indent=2, ensure_ascii=False), ex=900)
            except:
                logger.info("第" + str(i + 1) + "行解析浮窗报错了")
                continue
    # 关闭窗口
    driverUtil.closeHqew(handle)
    logger.info("数据缓存完成")
