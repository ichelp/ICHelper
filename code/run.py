from waitress import serve
import app
import logging
# import signal
# import threading
# from utils import logger
# from utils import DriverUtil

fmt = "%(asctime)s.%(msecs)03d %(levelname)s %(filename)s[line:%(lineno)d] %(message)s"
datefmt = "%Y-%m-%dT%H:%M:%S"
logging.basicConfig(level=logging.INFO, format=fmt, datefmt=datefmt, filename="var/log/IChelper.log", filemode="w")

# def handler(sig, xx):
#     logger.info('监听到退出信号 Got signal: ' + str(sig))
#     driver = DriverUtil.instance()
#     driver.quit()
#     logger.info('浏览器驱动关闭成功！')
# if isinstance(threading.current_thread(), threading._MainThread):
#     signal.signal(signal.SIGINT, handler)
    # signal.signal(signal.SIGINT, handler)

serve(app.app, host='127.0.0.1', port=7001)
