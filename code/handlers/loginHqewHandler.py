# THIS FILE IS SAFE TO EDIT. It will not be overwritten when rerunning go-raml.

from email import message
from flask import jsonify, request
from utils import logger
from utils import DriverUtil
from bs4 import BeautifulSoup
from parse.hqReptilePrase import _parseDetailContent
import time
import pytypes


def loginHqewHandler():
    logger.info("login hqew handler %s", request.json)
    commodityList = []
    message = "ok"
    try:
        reqBody = pytypes.LoginHqewReqBody(request.json)
        # logger.info("json load, username=%s, password=%s, handle=%s", reqBody.username, reqBody.password, reqBody.handle)
        driver = DriverUtil.instance()
        html, url, code, message = driver.loginHqew(reqBody.username, reqBody.password, reqBody.handle, reqBody.url)
        # html, url, code, message = driver.loginHqew(reqBody.username, reqBody.password, reqBody.handle, 'https://s.hqew.com/BAV99_10.html')
        if code != 401:
            # 获取文本
            contentBs = BeautifulSoup(html, features="lxml")
            if not contentBs:
                logger.error("未获取到contents")
                return jsonify()
            logger.info("获取到contents和driver")
            # 解析 url 获取料号和页码
            url = url[url.rfind("/", 0, len(url)) + 1: len(url)]
            redisCacheUrl = '/hqew/v1/products?page=1&q=' + str(url.split(".html")[0])
            commodityList, code, message = _parseDetailContent(contentBs, driver, reqBody.handle, redisCacheUrl)
    except:
        logger.exception("parse request body exception")
        code = 500
        message = "catch an exception"
    now = time.time()
    respBody = pytypes.LoginHqewRespBody(code=code, message=message, result=commodityList, timestamp=int(now))
    return jsonify(respBody.as_dict())
