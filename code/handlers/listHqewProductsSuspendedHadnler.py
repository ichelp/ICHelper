from email import message
from sqlite3 import Timestamp
from unittest import result
from flask import jsonify, request
import redis as redis
import pytypes
import time
import json

pool = redis.ConnectionPool(host='127.0.0.1', port=6379)
redisClient = redis.Redis(connection_pool=pool)


def listHqewProductsSuspendedHadnler():
    # get请求获取请求参数
    key = request.args.get("key")
    respBody = redisClient.get(key)
    if respBody is not None:
        # obj = json.loads(commodityList)
        # result = commodityList
        return respBody
    now = int(time.time())
    respBody = pytypes.ListHqewProductsRespBody(code=200, message="ok", result=[], timestamp=now)
    return jsonify(respBody.as_dict())
