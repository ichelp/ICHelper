# THIS FILE IS SAFE TO EDIT. It will not be overwritten when rerunning go-raml.

from email import message
from sqlite3 import Timestamp
from unittest import result
from flask import jsonify, request
import redis as redis
import pytypes
import time
import json

pool = redis.ConnectionPool(host='127.0.0.1', port=6379)
redisClient = redis.Redis(connection_pool=pool)


def listHqewProductsHandler():
    # get请求获取请求参数
    prefix_url = request.args.get("q").upper()
    suffix_url = request.args.get("page")
    # 精确查询
    flag = request.args.get('accurate')
    redisCacheUrl = '/hqew/v1/products?page=' + suffix_url + '&q=' + prefix_url + '&accurate=' + str(flag)+'&flag=info'
    # result = []
    respBody = redisClient.get(redisCacheUrl)
    if respBody is not None:
        # obj = json.loads(commodityList)
        # result = commodityList
        return respBody
    now = int(time.time())
    respBody = pytypes.ListHqewProductsRespBody(code=200, message="ok", result=[], timestamp=now)
    return jsonify(respBody.as_dict())
