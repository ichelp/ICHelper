import shelve
import time
from flask import request, jsonify
import pytypes
from utils import logger, DriverUtil

# 登录操作



def loginHqewStatusHandler():
    instance = DriverUtil.instance()
    flag = instance._flag
    now = time.time()
    logger.info("login hqew handler %s", request.json)
    # 判断登录状态
    if flag == 'true':
        logger.info("登录状态")
        respBody = pytypes.LoginHqewRespBody(code='200', message='登录成功', result=[], timestamp=int(now))
        return jsonify(respBody.as_dict())
    else:
        try:
            username = request.json['username']
            password = request.json['password']
            driver = instance.headLoginOpen('https://passport.hqew.com/login')
            time.sleep(1)
            u = driver.find_element_by_xpath('//*[@id="J_loginName"]')
            u.clear()
            u.send_keys(username)
            p = driver.find_element_by_xpath('//*[@id="J_loginPsw"]')
            p.clear()
            p.send_keys(password)
            b = driver.find_element_by_xpath('//*[@id="J_btnLogin"]')
            b.click()
            time.sleep(1)
            # 判断登录状态
            if 'https://ibsv3.hqew.com/' in driver.current_url:
                logger.info("登录状态")
                respBody = pytypes.LoginHqewRespBody(code=200, message='登录成功', result=[], timestamp=int(now))
                return jsonify(respBody.as_dict())
            else:
                respBody = pytypes.LoginHqewRespBody(code=401, message='账号密码错误', result=[], timestamp=int(now))
                return jsonify(respBody.as_dict())
        except:
            logger.exception("login hqew exception")
