# THIS FILE IS SAFE TO EDIT. It will not be overwritten when rerunning go-raml.
import datetime
from email import message

import redis
from flask import jsonify, request

from parse.hqReptilePrase import getAndParseContent
from utils.logging import logger
import pytypes
import time

url = 'https://s.hqew.com/'

pool = redis.ConnectionPool(host='127.0.0.1', port=6379)
redisClient = redis.Redis(connection_pool=pool)


def searchHqewProuctsHandler():
    a = datetime.datetime.now()
    logger.info("开始获取列表数据")
    # get请求获取请求参数
    # 关键词
    prefix_url = request.args.get("q").upper()
    # 页码
    suffix_url = request.args.get('page')
    # 精确查询
    flag = request.args.get('accurate')
    if flag == 'true':
        if suffix_url != '1':
            requestUrl = url + prefix_url + '______0_10_0_0_0_' + str(suffix_url) + '.html'
        else:
            requestUrl = url + prefix_url + '_10.html'
    else:
        requestUrl = url + prefix_url + '______0__0_0_0_' + str(suffix_url) + '.html'
    redisCacheUrl = '/hqew/v1/products?page=' + str(suffix_url) + '&q=' + prefix_url + '&accurate=' + str(flag)

    # 查询缓存
    products = redisClient.get(redisCacheUrl)
    if products is not None:
        b = datetime.datetime.now()
        logger.info("查询接口总耗时：" + str(b - a))
        return products
    else:
        commodityList, code, handle = getAndParseContent(requestUrl, redisCacheUrl)
        now = time.time()
        message = "ok"
        if code == 401:
            message = handle
        elif code == 403:
            message = handle
        elif code == 500:
            message = "parse content fail"
        respBody = pytypes.SearchHqewProuctsRespBody(code=code, message=message, result=commodityList,
                                                     timestamp=int(now), url=requestUrl)
        b = datetime.datetime.now()
        logger.info("查询接口总耗时：" + str(b - a))
        return jsonify(respBody.as_dict())
